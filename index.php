<?php

// подключаем автозагрузку классов
require __DIR__ . '/autoload.php';

// запускаем роутер
\App\Classes\Router::run();