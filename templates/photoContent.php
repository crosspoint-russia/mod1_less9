<div id="top_splash_box_container">
    <div id="top_splash_box">
        DM Photos<br>
    </div>
</div>

<div id="body_content">
    <div id="images_wallpaper_container">
        <?php foreach ($photos as $photo) : ?>
        <a class="fancybox photoholder" rel="lightbox[photo]" href="/uploads/photo/<?php echo $photo->getName(); ?>">
            <img src="/uploads/photo/<?php echo $photo->getName(); ?>" >
        </a>
        <?php endforeach; ?>

        <div style="clear: both;"></div>
    </div>
</div>

