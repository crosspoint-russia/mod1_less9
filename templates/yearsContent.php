<div id="top_splash_box_container">
    <div id="top_splash_box">
        DM Archives::<?php echo $askedYear; ?><br>
    </div>
</div>

<div id="body_content">
    <p>All Depeche Mode albums in <?php echo $askedYear; ?>:</p>
    <?php if (true !== empty($albums)) : ?>
    <div id="discography_singles_container">

        <?php foreach ($albums as $album) : ?>
            <div class="discography_singles_box">
                <div class="discography_singles_thumbnail">
                    <a href="/albums/show/<?php echo $album->getId(); ?>" title="<?php echo $album->getName(); ?>">
                        <img src="/uploads/albums/<?php echo $album->getCover(); ?>" alt="<?php echo $album->getName(); ?>" height="150" width="150">
                    </a>
                </div>
                <div class="discography_singles_text"><?php echo $album->getName(); ?><br>
                    &nbsp;<br>
                </div>
            </div>
        <?php endforeach; ?>

        <div style="clear: both;"></div>
    </div>
    <?php else: ?>
        <p><b>Depeche Mode does not released any albums in this year ;(</b></p>
        <p>Please try one of this:</p>
        <ul>
            <?php foreach ($years as $year): ?>
            <li><a href="/albums/year/<?php echo $year['value'] ?>"><?php echo $year['value'] ?></a></li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
</div>
