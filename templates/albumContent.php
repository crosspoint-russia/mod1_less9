<div id="top_splash_box_container">
    <div id="top_splash_box">
        <?php echo $album->getName(); ?><br>
    </div>

    <div id="sub_nav_splash_box">
        <ul>
            <li>Album released in <?php echo $album->getPubDate(); ?></li>
        </ul>
    </div>

</div>

<div id="body_content">
    <p><img src="/uploads/albums/<?php echo $album->getCover(); ?>" alt="<?php echo $album->getName(); ?>" id="mainPic"></p>
    <p><?php echo nl2br($album->getDescription()); ?></p>
</div>