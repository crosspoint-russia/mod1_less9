<div id="top_splash_box_container">
    <div id="top_splash_box">
        DM Albums<br>
    </div>

    <div id="sub_nav_splash_box">
        <ul>
            <li>Albums by year:</li>
            <?php foreach ($years as $year): ?>
                <li>
                    <a class="subnav_a" href="/albums/year/<?php echo $year['value']; ?>" title="releases in <?php echo $year['value']; ?>">
                        <?php echo $year['value']; ?>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>

</div>

<div id="body_content">
    <p>All Depeche Mode albums:</p>
    <div id="discography_singles_container">

        <?php foreach ($albums as $album) : ?>
            <div class="discography_singles_box">
                <div class="discography_singles_thumbnail">
                    <a href="/albums/show/<?php echo $album->getId(); ?>" title="<?php echo $album->getName(); ?>">
                        <img src="/uploads/albums/<?php echo $album->getCover(); ?>" alt="<?php echo $album->getName(); ?>" height="150" width="150">
                    </a>
                </div>
                <div class="discography_singles_text"><?php echo $album->getName(); ?><br>
                    &nbsp;<br>
                </div>
            </div>
        <?php endforeach; ?>

        <div style="clear: both;"></div>
    </div>
</div>
