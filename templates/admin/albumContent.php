<div class="page-header">
    <h1>Edit album:</h1>
</div>

<div class="row">
    <div class="col-md-12 text-left">

        <form enctype="multipart/form-data" action="/admin/store_album/" method="post">
            <div class="form-group">
                <label for="name">Album name</label>
                <input type="hidden" name="id" value="<?php echo $album->getId(); ?>">
                <input type="text" class="form-control" name="name" id="name" placeholder="ex.: Playing the Angel" value="<?php echo $album->getName(); ?>">
            </div>
            <div class="form-group">
                <label for="pub_date">Release date</label>
                <input type="datetime" class="form-control" name="pub_date" id="pub_date" placeholder="DD.MM.YYYY" value="<?php echo $album->getPubDate(); ?>">
            </div>
            <div class="form-group">
                <label for="cover">File input</label>
                <input type="file" name="cover" id="cover">
                <p class="help-block">only for JPG & PNG. Current cover:</p>
                <img src="/uploads/albums/<?php echo $album->getCover(); ?>" >
            </div>
            <div class="form-group">
                <textarea class="form-control" rows="3" placeholder="Album description" name="description"><?php echo $album->getDescription(); ?></textarea>
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
        </form>
    </div>


</div>