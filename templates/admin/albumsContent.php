<div class="page-header">
    <h1>Add new album:</h1>
</div>

<div class="row">
    <div class="col-md-12 text-left">

        <form enctype="multipart/form-data" action="/admin/store_album/" method="post">
        <div class="form-group">
            <label for="name">Album name</label>
            <input type="text" class="form-control" name="name" id="name" placeholder="ex.: Playing the Angel">
        </div>
        <div class="form-group">
            <label for="pub_date">Release date</label>
            <input type="datetime" class="form-control" name="pub_date" id="pub_date" placeholder="DD.MM.YYYY">
        </div>
        <div class="form-group">
            <label for="cover">File input</label>
            <input type="file" name="cover" id="cover">
            <p class="help-block">only for JPG & PNG.</p>
        </div>
        <div class="form-group">
            <textarea class="form-control" rows="3" placeholder="Album description" name="description"></textarea>
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
        </form>
    </div>


</div>



<div class="page-header">
    <h1>Stored albums:</h1>
</div>

<div class="row">
    <?php foreach ($albums as $album) : ?>
        <div class="col-md-2" style="height: 250px; overflow: hidden; margin-bottom: 20px;">
        <p>
            <?php echo $album->getName(); ?>
        </p>

            <a href="/admin/del_album/<?php echo $album->getId(); ?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> remove</a>
            <a href="/admin/edit_album/<?php echo $album->getId(); ?>"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> edit</a>


            <img src="/uploads/albums/<?php echo $album->getCover(); ?>" class="img-responsive">
        </div>
    <?php endforeach; ?>
</div>
