<div class="page-header">
    <h1>Upload photo:</h1>
</div>

<div class="row">
    <div class="col-md-12 text-left">
        <form enctype="multipart/form-data" action="/admin/add_photo/" method="post">
            <div class="form-group">
                <label for="exampleInputFile">File input</label>
                <input type="file" name="photoField">
                <p class="help-block">only for JPG & PNG.</p>
            </div>
            <button type="submit" class="btn btn-default">Submit</button>

        </form>
    </div>


</div>



<div class="page-header">
    <h1>Uploaded photos:</h1>
</div>

<div class="row">
    <?php foreach ($photos as $k => $photo) : ?>
    <div class="col-md-2" style="height: 130px; overflow: hidden; margin-bottom: 20px;">
        <span>
            <?php echo $photo->getName(); ?>
        </span>

        <a href="/admin/del_photo/<?php echo ++$k; ?>">
            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> remove
        </a>


        <img src="/uploads/photo/<?php echo $photo->getName(); ?>" class="img-responsive">
    </div>
    <?php endforeach; ?>
</div>
