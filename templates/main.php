<!DOCTYPE html>
<html lang="en">
<head>

    <title>Depeche Mode: The Archives</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="icon" type="image/ico" href="/templates/resources/favicon.ico">
    <link rel="stylesheet" href="/templates/resources/css/archive.css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:700,400" rel="stylesheet" type="text/css">
</head>

<body>
<b id="top_of_page"></b>
<div id="brat_nav_container">
    <div id="brat_nav">

        <div style="float: left;">
            <ul>
                <li><a class="nav_a_left" href="/" title="the start">Depeche Mode Discography</a></li>
            </ul>
        </div>
        <div style="float: right;">
            <ul>
                <li><a class="nav_a" href="/albums/" title="all of the releases of the band">Discography</a></li>
                <li><a class="nav_a" href="/photo/" title="publicity images, magazine scans, wallpaper and exclusives">Photo</a></li>
                <li><a class="nav_a" target="_blank" href="http://www.depechemode.com/" title="go to the main site...">Go to official site</a></li>
            </ul>
        </div>
    </div>
</div>

<?php echo $content; ?>

<div id="footer_1">
</div>
<div id="footer_2">
    <div id="footer_2a"><img src="/templates/resources/images/footer_bottom_crop.png" alt="depeche mode" style="margin: -23px auto;"><br>

        <p><b>Archives Curator:</b> <a class="broken_frame_footer" href = "mailto:webmaster@depechemode.com">Daniel Barassi</a></p>

        <p><a class="broken_frame_footer" target="_blank" href = "http://www.depechemode.com/">depeche mode dot com</a> |
            <a class="broken_frame_footer" target="_blank" href = "http://www.facebook.com/depechemode">facebook</a> |
            <a class="broken_frame_footer" target="_blank" href = "http://www.twitter.com/depechemode">twitter</a> |
            <a class="broken_frame_footer" target="_blank" href = "http://www.instagram.com/depechemode">instagram</a> |
            <a class="broken_frame_footer" target="_blank" href = "http://depechemode.tumblr.com/">tumblr</a></p>

        <p><a class="broken_frame_footer" href = "#top_of_page">Go back to the top of this page.</a></p>

    </div>
</div>

<div id="below_footer"><div id="below_footer_black"></div></div>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
<script src="/templates/resources/js/bgstretcher.js"></script>
<link rel="stylesheet" href="/templates/resources/css/jquery.fancybox.css?v=2.1.4" type="text/css" media="screen">
<script type="text/javascript" src="/templates/resources/js/jquery.fancybox.pack.js?v=2.1.4"></script>
<link rel="stylesheet" href="/templates/resources/css/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen">
<script type="text/javascript" src="/templates/resources/js/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="/templates/resources/js/jquery.fancybox-media.js?v=1.0.5"></script>
<link rel="stylesheet" href="/templates/resources/css/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen">
<script type="text/javascript" src="/templates/resources/js/jquery.fancybox-thumbs.js?v=1.0.7"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $(".fancybox").fancybox();
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        var heighty_window = $(window).height();
        var heighty_html = $('body').height();
        var the_difference = (heighty_window - heighty_html);

        if (heighty_html < heighty_window) {
            $('#below_footer_black').css('height', the_difference + 'px');
        }

    });
</script>

<script type="text/javascript">
    $(document).ready(function(){

        //  Initialize Background Stretcher
        $('#footer_2').bgStretcher({

            images: ['/templates/resources/images/broken_frame_footer.jpg'],

            imageWidth: 1600,
            imageHeight: 740,
            anchoring: 'bottom center',
            anchoringImg: 'bottom center',
            resizeProportionally: 'true',
            maxWidth: 'auto',
            maxHeight: 'auto'
        });
    });
</script>

</body>
</html>

