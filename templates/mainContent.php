<div id="top_splash_box_container">
    <div id="top_splash_box">
        DM Archives<br>
    </div>

    <div id="sub_nav_splash_box">
        <ul>
            <li>Albums by year:</li>
            <?php foreach ($years as $year): ?>
                <li>
                    <a class="subnav_a" href="/albums/year/<?php echo $year['value']; ?>" title="releases in <?php echo $year['value']; ?>">
                        <?php echo $year['value']; ?>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>

</div>

<div id="body_content">
    <p><img src="/templates/resources/images/dm2.jpg" alt="Depeche Mode" id="mainPic"></p>
    <p>Depeche Mode are an English synthpop band, founded in 1980, originally from the town of Basildon, Essex, United Kingdom. They are one of the most enduring and successful bands to have emerged during the 80s, and particularly from the new wave/new romantic era. The band name is derived from a French fashion magazine, Dépêche mode, which means "fashion dispatch."</p>

</div>