<?php

namespace App\Controllers;

use \App\Classes\Uploader as Uploader;
use \App\Models\Photos as Photos;
use \App\Models\Albums as Albums;

class Admin extends \App\Controllers\Controller
{
    public function __construct()
    {
        // наследуем родительский конструктор и переопределяем пути к шаблонам
        parent::__construct();
        $config = include(__DIR__ . '/../mainConfig.php');
        $this->templatePath = $config['adminTemplatePath'];
        $this->template = $this->templatePath . '/' . $config['adminTemplate'];
    }

    /**
     * Главная страница админ панели
     */
    public function index()
    {
        // шаблоны в текущем контроллере
        $contentTemplate = $this->templatePath . '/mainContent.php';
        // рендерим локальный контент
        $content = $this->view->render($contentTemplate);
        // показываем готовый шаблон
        $this->view->assign('content', $content)->display($this->template);

    }

    /**
     * Страница с формой добавления альбома и списком всех сохраненных альбомов
     */
    public function albums()
    {
        // шаблоны в текущем контроллере
        $contentTemplate = $this->templatePath . '/albumsContent.php';

        // загружаем модель Альбомов
        $albums = new Albums;

        // рендерим локальный контент
        $content = $this->view->assign('albums', $albums->getAllAlbums('DESC','id'))->render($contentTemplate);

        // показываем готовый шаблон
        $this->view->assign('content', $content)->display($this->template);

    }

    /**
     * принимает $_POST данные для добавления или обновления альбома
     */
    public function store_album()
    {
        if ('POST' === $_SERVER['REQUEST_METHOD']) {

            // Загружаем модель альбомов
            $albums = new Albums;

            // проверяем пришел ли через пост id альбома
            if (true === isset($_POST['id'])) {

                $id = (int) $_POST['id'];

                //пытаемся получить альбом с этим id
                $album = $albums->getAlbumById($id);

                // если метод getAlbumById вернул не пустой массив
                if (false === empty($album)) {
                    // вытаскиваем объект и избавляемся от массива
                    $album = $album[0];

                // иначе создаем пустую болванку альбома
                } else {
                    $album = new \App\Models\Album(null, '', '', 'none.png', '');
                }
            // если из пост не пришел id, создаем болванку альбома
            } else {
                $album = new \App\Models\Album(null, '', '', 'none.png', '');
            }

            // проверяем данные из пост и обновляем альбом

            if (true === isset($_POST['name'])) {
                $album->setName($_POST['name']);
            }
            if (true === isset($_POST['pub_date'])) {
                $pub_date = date('Y-m-d', strtotime($_POST['pub_date']));
                $album->setPubDate($pub_date);
            }
            if (true === isset($_POST['description'])) {
                $album->setDescription($_POST['description']);
            }
            
            $path = __DIR__ . '/../../uploads/albums';
            $uploader = new Uploader('cover');
            $uploadedFile = $uploader->setSavePath($path)->upload();
            
            if (false === is_null($uploadedFile)) {
                $album->setCover($uploadedFile);
            }

            // сохраняем альбом
            $albums->storeAlbum($album);
        }
        header('Location: /admin/albums/');
        die();
    }

    /**
     * Страница с формой редактирования альбома
     * @param $param
     */
    public function edit_album($param)
    {
        if (true === isset($param[0]))
        {
            $id = (int) $param[0];
            
            // шаблоны в текущем контроллере
            $contentTemplate = $this->templatePath . '/albumContent.php';

            // загружаем модель Альбомов
            $albums = new Albums;

            // рендерим локальный контент
            $content = $this->view->assign('album', $albums->getAlbumById($id)[0])->render($contentTemplate);

            // показываем готовый шаблон
            $this->view->assign('content', $content)->display($this->template);
            
        } else {
            header('Location: /admin/albums/');
            die();
        }

    }


    /**
     * Вызов метода удаления альбома по его ID 
     * @param $param
     */
    public function del_album($param)
    {
        if (true === isset($param[0]))
        {
            $id = (int) $param[0];
            $albums = new Albums;
            $albums->deleteRecordById($id);
        }
        header('Location: /admin/albums/');
        die();
    }

    /**
     * Страница с формой добавления фотографии и списком всех загруженных фото
     */
    public function photos()
    {
        // шаблоны в текущем контроллере
        $contentTemplate = $this->templatePath . '/photoContent.php';

        // модель для фоток
        $config = include(__DIR__ . '/../mainConfig.php');
        $photos = new Photos($config['photosPath']);

        // рендерим локальный контент
        $content = $this->view->assign('photos', $photos->getPhotos())->render($contentTemplate);

        // показываем готовый шаблон
        $this->view->assign('content', $content)->display($this->template);
    }

    /**
     * Принимает $_POST данные и загружает новое фото
     */
    public function add_photo()
    {
        if ('POST' === $_SERVER['REQUEST_METHOD']) {
            $path = __DIR__ . '/../../uploads/photo';
            $uploader = new Uploader('photoField');
            $uploader->setSavePath($path)->upload();
        }
        header('Location: /admin/photos/');
        die();
    }

    /**
     * Вызов метода удаления фото по его ID
     * @param $param
     */
    public function del_photo($param)
    {
        if (true === isset($param[0]))
        {
            $id = (int) $param[0];
            $config = include(__DIR__ . '/../mainConfig.php');
            $photos = new Photos($config['photosPath']);
            $photos->deleteById($id);
        }
        header('Location: /admin/photos/');
        die();
    }
}