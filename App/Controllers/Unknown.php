<?php

namespace App\Controllers;


class Unknown extends \App\Controllers\Controller
{

    /**
     * Страница ошибки 404
     */
    public function index()
    {
        header('HTTP/1.0 404 Not Found');
        // шаблон в текущем контроллере
        $contentTemplate = $this->templatePath . '/404Content.php';

        // рендер локального вида
        $content = $this->view->render($contentTemplate);

        // показываем готовый шаблон
        $this->view->assign('content', $content)->display($this->template);
    }
}