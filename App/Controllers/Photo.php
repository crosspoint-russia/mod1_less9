<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 07.01.2017
 * Time: 13:43
 */

namespace App\Controllers;

use \App\Models\Photos as Photos;

class Photo extends \App\Controllers\Controller
{
    /**
     * Страница фотогалереи
     */
    public function index()
    {

        // шаблоны в текущем контроллере
        $contentTemplate = $this->templatePath . '/photoContent.php';

        // модель для фоток
        $config = include(__DIR__ . '/../mainConfig.php');
        $photos = new Photos($config['photosPath']);

        // рендерим локальный контент
        $content = $this->view->assign('photos', $photos->getPhotos())->render($contentTemplate);

        // показываем готовый шаблон
        $this->view->assign('content', $content)->display($this->template);

    }
}