<?php

namespace App\Controllers;

use \App\Classes\View as View;

class Controller
{
    protected $view;
    protected $templatePath;
    protected $template;

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        $this->view = new View();

        // загружаем конфиг и сохраняем общую папку шаблонов, главный шаблон
        $config = include(__DIR__ . '/../mainConfig.php');
        $this->templatePath = $config['templatePath'];
        $this->template = $this->templatePath . '/' . $config['template'];
    }

    // заглушка для дочерних контроллеров
    public function index()
    {
    }
    
}