<?php

namespace App\Controllers;

use \App\Models\Albums as Albums;

class Main extends \App\Controllers\Controller
{
    /**
     * Главная страница
     */
    public function index() {
        
        // шаблоны в текущем контроллере
        $contentTemplate = $this->templatePath . '/mainContent.php';
        
        // модель Альбомов
        $albums = new Albums;
        
        // рендерим локальный контент
        $content = $this->view->assign('years', $albums->getAlbumsYears())->render($contentTemplate);
        
        // показываем готовый шаблон
        $this->view->assign('content', $content)->display($this->template);
    }
}