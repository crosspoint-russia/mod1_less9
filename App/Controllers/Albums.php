<?php

namespace App\Controllers;

class Albums extends \App\Controllers\Controller
{
    public function index()
    {
        // шаблон в текущем экшене
        $contentTemplate = $this->templatePath . '/albumsContent.php';

        // загружаем модель Альбомов
        $albums = new \App\Models\Albums;

        // рендерим локальный шаблон
        $content = $this->view
            ->assign('albums', $albums->getAllAlbums())
            ->assign('years', $albums->getAlbumsYears())
            ->render($contentTemplate);

        // показываем готовый глобальный шаблон
        $this->view->assign('content', $content)->display($this->template);
    }

    /**
     * Страница для отобрадения альбомов по году
     * @param $params
     */
    public function year($params)
    {
        $contentTemplate = $this->templatePath . '/yearsContent.php';

        if (true === isset($params[0])) {
            $date = (int) $params[0];
        } else {
            header('Location: /404');
            die();
        }

        $albums = new \App\Models\Albums;

        $content = $this->view
            ->assign('albums', $albums->getAlbumsByYear($date))
            ->assign('years', $albums->getAlbumsYears())
            ->assign('askedYear', $date)
            ->render($contentTemplate);

        $this->view->assign('content', $content)->display($this->template);
    }

    /**
     * Страница с подробной информацией об альбоме
     * @param $params
     */
    public function show($params)
    {
        $contentTemplate = $this->templatePath . '/albumContent.php';
        
        if (true === isset($params[0])) {
            $id = (int) $params[0];
        } else {
            header('Location: /404');
            die();
        }

        $albums = new \App\Models\Albums;
        $album = $albums->getAlbumById($id);
        
        if (true === empty($album)) {
            header('Location: /404');
            die();
        }

        $content = $this->view
            ->assign('album', $album[0])
            ->render($contentTemplate);

        $this->view->assign('content', $content)->display($this->template);

    }
}