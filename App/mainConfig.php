<?php

// конфиг для различных настроек приложения.
// пути к главным шаблонам, сами шаблоны, пути к файлам фотогалереи

return [
    'templatePath' => __DIR__ . '/../templates',
    'adminTemplatePath' => __DIR__ . '/../templates/admin',
    'template' => 'main.php',
    'adminTemplate' => 'main.php',
    'photosPath' => __DIR__ . '/../uploads/photo',
];