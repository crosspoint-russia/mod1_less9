<?php

namespace App\Classes;

class View
{
    protected $data = [];

    /**
     * @param $name
     * @param $value
     * @return $this
     */
    public function assign($name, $value)
    {
        $this->data[$name] = $value;
        return $this;
    }

    /**
     * @param $template
     */
    public function display($template)
    {
        foreach ($this->data as $key => $value) {
            $$key = $value;
        }
        include $template;
    }

    /**
     * @param $template
     * @return string
     */
    public function render($template)
    {
        foreach ($this->data as $key => $value) {
            $$key = $value;
        }
        ob_start();
        include $template;
        $result = ob_get_contents();
        ob_end_clean();
        return $result;
    }
}