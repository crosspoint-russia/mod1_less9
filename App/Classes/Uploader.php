<?php

namespace App\Classes;

class Uploader
{
    protected $fieldName;
    protected $fileData;
    protected $savePath = __DIR__ . '/../../uploads';

    /**
     * Uploader constructor.
     * @param $fieldName
     */
    public function __construct($fieldName)
    {
        $this->fieldName = $fieldName;
    }

    /**
     * 
     * Метод isUploaded() проверяет - был ли загружен файл от данного имени поля
     * 
     * @return bool
     */
    protected function isUploaded()
    {
        if (isset($_FILES[$this->fieldName])) {
            if (0 == $_FILES[$this->fieldName]['error']) {
                $this->fileData = $_FILES[$this->fieldName];
                return true;
            } else {
                return false;
            }

        } else {
            return false;
        }
    }

    /**
     *
     * Метод для проверки того является ли загруженный файл картинкой
     *
     * @return bool
     */
    protected function isImage()
    {
        if (true === isset($this->fileData['type'])) {
            if ('image/png' === $this->fileData['type'] || 'image/jpeg' === $this->fileData['type']) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * 
     * Метод upload() осуществляет перенос файла (если он был загружен!) из временного места в постоянное
     * 
     * @return null
     */
    public function upload()
    {
        if (true === $this->isUploaded() && true === $this->isImage()) {
            if (move_uploaded_file($this->fileData['tmp_name'], $this->savePath . '/' . $this->fileData['name'])) {
                return $this->fileData['name'];
            } else {
                return null;
            }

        } else {
            return null;
        }

    }

    /**
     * 
     * метод для установки папки сохранения
     * 
     * @param $path
     * @return $this
     */
    public function setSavePath($path)
    {
        $this->savePath = $path;
        return $this;
    }

}