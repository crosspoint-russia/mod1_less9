<?php

namespace App\Classes;

class DB
{
    protected $dbh = null;

    /**
     * DB constructor.
     */
    public function __construct()
    {
        $config = include(__DIR__ . '/../dbConfig.php');
        $dsn = $config['driver'] . ':host=' . $config['host'] . ';dbname=' . $config['dbname'];
        $this->dbh = new \PDO($dsn, $config['username'], $config['pass']);
    }

    /**
     * @param string $sql
     * @return bool
     */
    public function execute(string $sql, $data=[])
    {
        $sth = $this->dbh->prepare($sql);
        return $sth->execute($data);
    }

    /**
     * @param string $sql
     * @param array $data
     * @return array|bool
     */
    public function query(string $sql, array $data)
    {
        // готовим запрос
        $sth = $this->dbh->prepare($sql);

        // запускаем запрос, execute возвращает bool начение
        if (true === $sth->execute($data)) {
            // если всё успешно выполнилось, возвращаем результат
            return $sth->fetchAll(\PDO::FETCH_ASSOC);
        } else {
            return false;
        }
    }

}