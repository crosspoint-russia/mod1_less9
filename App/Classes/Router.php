<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 07.01.2017
 * Time: 14:18
 */

namespace App\Classes;


class Router
{
    public static function run(){
        // получаем URI и обрезаем ненужные /
        $currentLocation = $_SERVER['REQUEST_URI'];
        $currentLocation = trim($currentLocation, '/');

        // контроллер и действие по умолчанию
        $controller = '\App\Controllers\Main';
        $action = 'index';
        $params = [];

        // если мы не на главной странице
        if ('' !== $currentLocation) {
            $askedRoute = null;
            $askedAction = null;
            
            // разбиваем URI в массив, загружаем разрешенные маршруты
            $explodedLocation = explode('/', $currentLocation);
            $allowedRoutes = include(__DIR__ . '/../routes.php');

            // получаем запрошенный маршрут, проверяем его в массиве разрешенных и выбираем контроллер
            if (false === empty($explodedLocation)) {
                
                // извлекаем элемент из массива с запрошенным маршрутом.
                // проверяем есть ли такой маршрут в разрешенных 
                $askedRoute = array_shift($explodedLocation);
                
                // если маршрут разрешен, загружаем имя контроллера.
                // иначе загружаем контроллер для 404 ошибки
                if (true === isset($allowedRoutes[$askedRoute])) {
                    $controller = $allowedRoutes[$askedRoute];
                } else {
                    $controller = $allowedRoutes['404'];
                }
            }

            // проверяем не опустел ли массив с запрошенным маршрутом
            // получаем запрошенный action, проверяем на возможность вызова.
            // Если недоступно, загружаем контроллер для 404 ошибки
            if (false === empty($explodedLocation)) {
                $askedAction = array_shift($explodedLocation);
                if (method_exists($controller, $askedAction)) {
                    $action = $askedAction;
                } else {
                    $controller = $allowedRoutes['404'];
                }
            }

            // если что-то осталось от массива с запрошенным маршрутом,
            // передаем остатки как параметр к action
            if (false === empty($explodedLocation)) {
                $params = $explodedLocation;
            }
        }
        
        // инициализируем контроллер, запускаем экшн, прокидываем в него параметры
        $page = new $controller;
        $page->$action($params);
    }
}