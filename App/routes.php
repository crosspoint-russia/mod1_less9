<?php

// разрешенные маршруты и контроллеры к ним

return [
    'albums'=>'\App\Controllers\Albums',
    'photo'=>'\App\Controllers\Photo',
    'admin'=>'\App\Controllers\Admin',
    '404'=>'\App\Controllers\Unknown',
];