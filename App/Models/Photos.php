<?php

namespace App\Models;

use \App\Models\Photo as Photo;

class Photos
{
    protected $photos = [];
    protected $path;

    /**
     * Модель коллекции фотографий
     * Photos constructor.
     * @param $path
     */
    public function __construct($path)
    {
        $this->path = $path;
        $excludeRecords = ['.','..'];
        $tempArray = scandir($path);

        $files = array_diff($tempArray, $excludeRecords);

        foreach ($files as $file) {
            $this->photos[] = new Photo($file);
        }
    }

    /**
     * @return array
     */
    public function getPhotos()
    {
        return $this->photos;
    }

    /**
     * Удаляет фото по ID из папки и внутреннего массива
     * @param $id
     * @return bool
     */
    public function deleteById($id) {
        $id = $id-1;
        if (true === isset($this->photos[$id])) {
            if (true === unlink($this->path . '/' . $this->photos[$id]->getName())) {
                unset($this->photos[$id]);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

}