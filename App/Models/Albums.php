<?php

namespace App\Models;

use \App\Models\Album as Album;

class Albums extends \App\Models\Model
{


    /**
     *
     * Получить все альбомы
     *
     * @param string $orderDirection
     * @param string $orderBy
     * @return Album[]
     */
    public function getAllAlbums($orderDirection = 'DESC', $orderBy = 'pub_date')
    {
        $records = $this->getAllRecords($orderDirection, $orderBy);
        return $this->buildAlbums($records);
    }

    /**
     *
     * Получить альбомы по году
     *
     * @param int $year
     * @return Album[]
     */
    public function getAlbumsByYear(int $year)
    {
        $dates = [];
        $dates['startDate'] = date('Y-m-d', strtotime($year . '-01-01'));
        $dates['endDate'] = date('Y-m-d', strtotime($year . '-01-01 +365 days'));

        $sql = 'SELECT * FROM ' . $this->table . ' WHERE pub_date>=:startDate AND pub_date<=:endDate ORDER BY pub_date DESC';
        $tempArray = $this->prepareArray($dates);

        $records = $this->db->query($sql, $tempArray);
        return $this->buildAlbums($records);
    }

    /**
     *
     * Получить альбом по id
     *
     * @param int $id
     * @return Album[]
     */
    public function getAlbumById(int $id)
    {
        $record = $this->getRecordById($id);
        return $this->buildAlbums($record);
    }

    /**
     * 
     * Получить список доступных для выборки лет
     * 
     * @return array|bool
     */
    public function getAlbumsYears()
    {
        $sql = 'SELECT YEAR(pub_date) AS value FROM ' . $this->table . ' GROUP BY pub_date';
        return $this->db->query($sql, []);
    }


    /**
     * 
     * Сохранение альбома в БД
     * 
     * @param \App\Models\Album $album
     * @return mixed
     */
    public function storeAlbum(Album $album)
    {
        $data = $album->getAlbumData();
        if (true === is_null($data['id'])) {
            return $this->insertRecord($data);
        } else {
            $recordId = $data['id'];
            unset($data['id']);
            return $this->updateRecordById($recordId, $data);
        }
    }

    /**
     *
     * Внутренний метод для штамповки альбомов из записей
     *
     * @param array $records
     * @return Album[]
     */
    protected function buildAlbums(array $records)
    {
        $albums = [];

        if (false === empty($records)) {
            foreach ($records as $record) {
                $albums[] = new Album($record['id'], $record['name'], $record['pub_date'], $record['cover'], $record['description']);
            }
        }

        return $albums;
    }
}