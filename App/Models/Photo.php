<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 07.01.2017
 * Time: 18:28
 */

namespace App\Models;


class Photo
{
    protected $name;
    
    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }
    
}