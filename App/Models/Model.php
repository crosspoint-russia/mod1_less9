<?php

namespace App\Models;

class Model
{
    // защищенное свойство объекта для хранения данных
    protected $table;
    protected $db;

    // определяем таблицу для работы, создаем и сохраняем в себе экземпляр DB
    public function __construct()
    {
        $tempArray = explode('\\', get_class($this));
        $className = array_pop($tempArray);
        $this->table = strtolower($className);
        $this->db = new \App\Classes\DB;
    }

    /**
     *
     * Генерация массива для подготовленного запроса
     *
     * @param $data
     * @return array
     */
    protected function prepareArray($data)
    {
        $queryData = [];
        foreach ($data as $key => $value) {
            $queryData[':' . $key] = $value;
        }
        return $queryData;
    }

    /**
     *
     * Получение всех записей из таблицы
     *
     * @param string $orderDirection
     * @param string $orderBy
     * @return mixed
     */
    public function getAllRecords($orderDirection = 'DESC', $orderBy = 'id')
    {
        $sql = 'SELECT * FROM ' . $this->table . ' ORDER BY :orderBy :orderDirection';
        return $this->db->query($sql, [
            ':orderBy'=> $orderBy,
            ':orderDirection'=> $orderDirection,
            ]);
    }

    /**
     *
     * Получение записи по id
     *
     * @param int $id
     * @return mixed
     */
    public function getRecordById(int $id)
    {
        return $this->db->query('SELECT * FROM ' . $this->table . ' WHERE id=:id', [':id'=>$id]);
    }

    /**
     * 
     * Обновить запись в БД по id
     * 
     * @param int $id
     * @param array $data
     * @return mixed
     */
    public function updateRecordById(int $id, array $data)
    {
        $queryString = 'UPDATE ' . $this->table . ' SET ';

        $tempArray = [];
        foreach ($data as $key => $value) {
            $tempArray[] = $key . '=:' . $key;
        }
        $queryString .= implode(', ', $tempArray);

        $queryString .= ' WHERE id=:id';

        $queryData = $this->prepareArray($data);
        $queryData[':id'] = $id;

        return $this->db->execute($queryString, $queryData);
    }

    /**
     * 
     * Вставить запись в БД
     * 
     * @param array $data
     * @return mixed
     */
    public function insertRecord(array $data)
    {
        $queryString = 'INSERT INTO ' . $this->table . ' (';
        $queryString .= implode(', ', array_keys($data)) . ') VALUES (';

        $tempArray = [];
        foreach ($data as $key => $value) {
            $tempArray[] = ':' . $key;
        }
        $queryString .= implode(', ', $tempArray);

        $queryString .= ')';
        $queryData = $this->prepareArray($data);

        return $this->db->execute($queryString, $queryData);
    }

    /**
     * 
     * Удалить запись из БД по id
     * 
     * @param $id
     * @return mixed
     */
    public function deleteRecordById($id)
    {
        return $this->db->execute('DELETE FROM ' . $this->table . ' WHERE id=:id', [':id'=>$id]);
    }


}