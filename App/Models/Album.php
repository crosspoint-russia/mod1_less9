<?php

namespace App\Models;


class Album 
{
    protected $id;
    protected $name;
    protected $pub_date;
    protected $cover;
    protected $description;

    /**
     * Album constructor.
     * @param $id
     * @param $name
     * @param $pub_date
     * @param $cover
     * @param $description
     */
    public function __construct($id, $name, $pub_date, $cover, $description)
    {
        $this->id = $id;
        $this->name = $name;
        $this->pub_date = $pub_date;
        $this->cover = $cover;
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPubDate()
    {
        $date = strtotime($this->pub_date);
        return date('d.m.Y', $date);
    }


    /**
     * @param $pub_date
     * @return $this
     */
    public function setPubDate($pub_date)
    {
        $this->pub_date = $pub_date;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCover()
    {
        return $this->cover;
    }


    /**
     * @param $cover
     * @return $this
     */
    public function setCover($cover)
    {
        $this->cover = $cover;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }


    /**
     * @param $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Метод для возврата всех свойств массивом
     * @return array
     */
    public function getAlbumData()
    {
        return get_object_vars($this);
    }

}