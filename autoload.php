<?php

function __autoload($className)
{

    $className = str_replace('\\', '/', $className);
    $fileName = __DIR__ . '/' . $className . '.php';

    if (true === file_exists($fileName)) {
        require $fileName;
    }

}